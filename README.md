# AutomationChallenge

Start an NginX Server 

###Assumptions
* Gitlab runner is configured to access some form of Cloud env
* Certs are available, externally managed and stored in some kind of vault

* minikube has ingress activated
* entry in hosts

create tls certs: 
`docker build --pull --no-cache -t tls . &&
 docker run -v $(pwd)/certs/:/app tls`

`curl -k https://www.karl.com`
