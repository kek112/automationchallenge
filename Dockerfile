FROM alpine
RUN apk add --no-cache --update openssl
WORKDIR /app
CMD openssl req -x509 -newkey rsa:4096 -sha256 -nodes -keyout tls.key -out tls.crt -subj "/CN=karl.com" -days 365